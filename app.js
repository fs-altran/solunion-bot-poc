var restify = require('restify');
var builder = require('botbuilder');
var botbuilder_azure = require('botbuilder-azure');
var config = require('./config/constants');
var logger = require('./utils/logger');
var server = restify.createServer();
var intents = require('./intents/index');
var login = require('./intents/login');
var nointent = require('./intents/nointent');
var callservice = require('./intents/callservice');
var pending = require('./intents/pending');
var removed = require('./intents/removed');
var welcome = require('./intents/welcome');
var nplStart = require('./intents/nlpStart');
var other = require('./intents/other_query');

//Config bot
var tableName = 'botdata';
var azureTableClient = new botbuilder_azure.AzureTableClient(tableName, process.env['AzureWebJobsStorage']);
var tableStorage = new botbuilder_azure.AzureBotStorage({ gzipData: false }, azureTableClient);

var settingsOfBot = config.bot;
var connector = new builder.ChatConnector(settingsOfBot);
var bot = new builder.UniversalBot(connector);
bot.set('storage', tableStorage);

//Define dialogs
bot.dialog('/', intents.get());
welcome.initDialog(bot);
login.initDialog(bot);
callservice.initDialog(bot);
nplStart.initDialog(bot);
pending.initDialog(bot);
removed.initDialog(bot);
nointent.initDialog(bot);
other.initDialog(bot);

bot.use({
  receive: function (event, next) {
    logger.info('user: ' + event.text)
    next();
  }
});

server.get(/.*/, restify.serveStatic(config.server));
server.post('/api/messages', connector.listen());
server.listen(process.env.port || process.env.PORT || 3978, function () {
  logger.debug('%s listening to %s', server.name, server.url);
});