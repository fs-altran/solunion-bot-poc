module.exports = {
  server: {
    'directory': '.',
    'default': 'index.html'
  },
  bot: {
    appId: process.env.MicrosoftAppId,
    appPassword: process.env.MicrosoftAppPassword,
  },
  // url_api: 'https://f3ra6dlzkd.execute-api.eu-west-1.amazonaws.com/v0_1',
  url_api: 'https://www.crediseguro.com.co/crediseguro/ws',

  luis: {
    url: "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/d26c6c97-28ae-4628-8088-84101abac5ca?subscription-key=29d8ec01c9a045b488c21196854f05d9&staging=true&verbose=true&timezoneOffset=0&q=",
    minScore: 0.7
  },
  logger:{
    name:'BotSolunion',
    nivel:'DEBUG'
  },
  list: {
    listState: ['PENDIENTE', 'REVISION', 'COMPLETO', 'LIMITADO', 'REDUCIDO', 'EXCLUIDO', 'CANCELADO', 'ELIMINADO'],
    listCause: ['El cupon solicitado es elevado', 'La solicitud financiera de la empresa', 'confidencial o riesgo general', 'otras causas']

  }
}