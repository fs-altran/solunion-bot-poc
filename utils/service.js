const config = require('../config/constants');
var axios = require('axios');
var logger = require('./logger');


// Call external services
module.exports.getPolicies = function getPolicies(cod, codDeudor, user, pass, callback) {
  let url = config.url_api + '/cupos/buscarCupos?'+'user=' + user + '&password='+pass + '&codPoliza=' + cod + '&codDeudor='+codDeudor+'&codCompania=1&palabraBusqueda=&registroDesde=1&cantidadRegistros=20';
  axios.get(url)
    .then(function (response) {
      //logger.debug('Los datos se han recogido y enviado correctamente.');
      callback(null, response.data);
    }).catch(function (error) {
      //logger.debug('Se ha producido un error al recoger los datos.');
      callback({'error':error,'url':url});
    });
};

module.exports.validCode = function validCode(debtor_cod, policy_cod, user, pass,callback) {
  let url =config.url_api + '/especificos/informacionDeudor?' +'user=' + user + '&password='+pass + '&codCompania=1' + '&codDeudor=' + debtor_cod;
  axios.get(url)
    .then(function (response) {
      logger.debug('Los datos se han recogido y enviado correctamente.');
      callback(null, response.data);
    }).catch(function (error) {
      logger.debug('Se ha producido un error al recoger los datos.');
      callback({'error':error,'url':url});
    });
};
